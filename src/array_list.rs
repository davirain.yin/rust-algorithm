pub struct ArrayList<T> {
    arr: Vec<T>,
    len: usize,
}

impl<T> ArrayList<T> {
    pub fn new() -> Self {
        Self {
            arr: Vec::new(),
            len: 0usize,
        }
    }

    pub fn get_length(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0usize
    }

    pub fn push(&mut self, value: T) {
        println!("ArrayList len : {}", self.len);
        if self.len == 0usize {
            self.len += 1;
            self.arr.push(value);
        } else {

        }

    }

}


mod tests {
    use crate::array_list::ArrayList;

    #[test]
    fn test_array_list_is_empty() {
        let empty_array_list = ArrayList::<i32>::new();

        assert_eq!(true, empty_array_list.is_empty());
    }
}


