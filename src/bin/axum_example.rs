use axum::prelude::*;
use axum::extract::Json;
use std::net::SocketAddr;
use serde::Deserialize;
use log::info;

#[derive(Deserialize, Debug)]
struct CreateUser {
    username: String,
}

async fn create_user(Json(payload): Json<CreateUser>) {
    // `payload` is a `CreateUser`
    println!("User: {:?}", payload);
    info!("User: {:?}", payload);
    println!("User name: {}", payload.username);
    info!("User name : {}", payload.username);
}


#[tokio::main]
async fn main() {
    env_logger::init();

    let app = route("/", get(handler))
            .route("/user", post(create_user));


    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    log::info!("addr: {}", addr);

    tracing::debug!("listening on {}", addr);

    hyper::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn handler() -> response::Html<&'static str> {
    response::Html("<h1>Hello, World!</h1>")
}